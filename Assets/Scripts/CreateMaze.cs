﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateMaze : MonoBehaviour {

    public static int width { get; private set; }
    public static int height { get; private set; }
    List<Slot> _maze = new List<Slot>();
    Stack<Slot> _creatingMaze = new Stack<Slot>();
    Slot _firstSlot;
    Slot _currentSlot;
    Slot _nextSlot;

    public void SetValueWith(Text t){
        int x;
        if (Int32.TryParse(t.text, out x))
            height = Mathf.Abs(x);
    }

    public void SetValueHeigt(Text t){
        int y;
        if (Int32.TryParse(t.text, out y))
            width = Mathf.Abs(y);
    }

    public void EvMaze(){
        EvClear();
        EvCreateSlots();
        EvCreateMaze();
        EvSolveMaze();
    }

    void EvSolveMaze(){
        //BFS
        if (_maze.Count > 0){

            //Reset visits.
            foreach (var item in _maze)
                item.visited = false;

            //Define the beginning and end.
            Slot start = _maze[0];
            Slot end = _maze[width * height - 1];
            Queue<Slot> S = new Queue<Slot>();

            //Set the origin.
            S.Enqueue(start);
            start.visited = true;

            while (S.Count > 0){

                Slot V = S.Dequeue();

                if (V == end){
                    paitingRoad(end);
                    break;
                }

                foreach (var W in V.road){
                    if (!W.visited){
                        W.visited = true;
                        W.father = V;//Set father of road.
                        S.Enqueue(W);
                    }
                }
            }
        }
    }

    //The way we went.
    private void paitingRoad(Slot slot){
        slot.text.color = new Color(1,0,0);
        slot.GetComponent<Renderer>().material.color = new Color(0, 1, 0);

        if (slot.father == null)
            return;

        paitingRoad(slot.father);
    }

    void EvCreateMaze(){
        //DFS
        if (_maze.Count > 0){

            _firstSlot = _maze[0];
            _currentSlot = _firstSlot;
            _currentSlot.visited = true;

            while (MissingToVisit(_maze)){
                if (MissingToVisit(_currentSlot.links)){
                    //Select the next slot.
                    do{
                        _nextSlot = _currentSlot.links[UnityEngine.Random.Range(0, _currentSlot.links.Count)];
                    } while (_nextSlot.visited == true);

                    _creatingMaze.Push(_currentSlot);
                    //Remove walls.
                    _currentSlot.RemoveWalls(_nextSlot);
                    //Update current slot.
                    _currentSlot.road.Add(_nextSlot);
                    _currentSlot = _nextSlot;
                    _currentSlot.visited = true;

                }else if (_creatingMaze.Count > 0){
                    _currentSlot = _creatingMaze.Pop();
                }
            }
        }
    }


    void EvCreateSlots() {
        for (int x = width; 0 < x; x--) {
            for (int y = height; 0 < y; y--) {
                var a = Factory.Instance.Create(Factory.FactoryID.Slot, new Vector3(x, y, 0), new Quaternion(0.7f, 0, 0, -0.7f));
                a.GetComponentInChildren<TextMesh>().text = (y.ToString() + "-" + x.ToString());
                _maze.Add(a.GetComponent<Slot>());
            }
        }
        //Add neighbors in the slots.
        foreach (var i in _maze){
            foreach (var j in _maze){
                if (i.pos[0] - 1 == j.pos[0] && i.pos[1] == j.pos[1])
                    i.links.Add(j);
                else if (i.pos[0] + 1 == j.pos[0] && i.pos[1] == j.pos[1])
                    i.links.Add(j);
                else if (i.pos[1] - 1 == j.pos[1] && i.pos[0] == j.pos[0])
                    i.links.Add(j);
                else if (i.pos[1] + 1 == j.pos[1] && i.pos[0] == j.pos[0])
                    i.links.Add(j);
            }
        }
        print(_maze.Count);
    }

    public void EvClear(){

        if (_maze.Count > 0){
            _firstSlot = null;
            for (int i = 0; i < _maze.Count; i++){
                DestroyImmediate(_maze[i]);
                _maze[i] = null;
            }
        }

        _nextSlot = _firstSlot = null;
        _creatingMaze.Clear();
        _maze.Clear();
        _maze = new List<Slot>();
        print(_maze.Count);
    }

    bool MissingToVisit(List<Slot> refence){
        foreach (var item in refence)
            if (item.visited == false)
                return true;
        return false;
    }
}
