﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public Camera camera;
    Vector3 _reference;
    Vector3 _newPosition;

    public void Update(){

        if (camera != null){

            if (Input.GetAxis("Mouse ScrollWheel") > 0)
                camera.orthographicSize -= Input.GetAxis("Mouse ScrollWheel");
            if (Input.GetAxis("Mouse ScrollWheel") < 0)
                camera.orthographicSize -= Input.GetAxis("Mouse ScrollWheel");

            if (Input.GetMouseButtonDown(2))
                _reference = Input.mousePosition;

            if (Input.GetMouseButton(2)){
                _newPosition = _reference - Input.mousePosition;
                camera.transform.position += _newPosition / 1000;
            }
        }
    }
}
