﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slot : MonoBehaviour {

    public int[] pos = new int[2];
    public List<Slot> links;
    public List<Slot> road;//Agregado.
    public bool visited;
    public Slot father { get; set; }//Agregado.

    public GameObject wallTop;
    public GameObject wallBot;
    public GameObject wallLeft;
    public GameObject wallRight;
    public TextMesh text;

    void Awake(){
        wallTop = transform.GetChild(0).gameObject;
        wallBot = transform.GetChild(1).gameObject;
        wallRight = transform.GetChild(2).gameObject;
        wallLeft = transform.GetChild(3).gameObject;
        pos[0] = (int)transform.position.x;
        pos[1] = (int)transform.position.y;
    }

    public void RemoveWalls(Slot b){
        Transform tA = GetComponent<Transform>();
        Transform tB = b.GetComponent<Transform>();

        if (tA.localPosition.x > tB.localPosition.x){
            Destroy(wallLeft);
            Destroy(b.wallRight);
        }
        else if (tA.localPosition.x < tB.localPosition.x){
            Destroy(b.wallLeft);
            Destroy(wallRight);
        }

        else if (tA.localPosition.y < tB.localPosition.y){
            Destroy(b.wallBot);
            Destroy(wallTop);
        }
        else if (tA.localPosition.y > tB.localPosition.y){
            Destroy(wallBot);
            Destroy(b.wallTop);
        }
    }

    public void OnDestroy(){
        Destroy(this.gameObject);
    }
}
